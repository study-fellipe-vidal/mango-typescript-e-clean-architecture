import { SavePurchases } from '@/domain/usecases'

export const mockPurchases = (): Array<SavePurchases.Input> => [
  { id: '1', data: new Date(), value: 50 },
  { id: '2', data: new Date(), value: 20 }
]
