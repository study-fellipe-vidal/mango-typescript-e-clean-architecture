import { SavePurchases } from '@/domain/usecases'
import { CacheStore } from '../protocols/cache'

export class CacheStoreSpy implements CacheStore {
  messages: Array<CacheStoreSpy.Message> = []
  deleteKey?: string
  insertKey?: string
  insertValues: Array<SavePurchases.Input> = []

  async delete(key: string) {
    this.deleteKey = key
    this.messages.push(CacheStoreSpy.Message.delete)
  }

  async insert(key: string, value: any) {
    this.insertKey = key
    this.insertValues = value
    this.messages.push(CacheStoreSpy.Message.insert)
  }

  async replace(key: string, value: any) {
    this.delete(key)
    this.insert(key, value)
  }

  throwDeleteError() {
    jest.spyOn(CacheStoreSpy.prototype, 'delete').mockImplementationOnce(() => {
      this.messages.push(CacheStoreSpy.Message.delete)
      throw new Error()
    })
  }
  throwInsertError() {
    jest.spyOn(CacheStoreSpy.prototype, 'insert').mockImplementationOnce(() => {
      this.messages.push(CacheStoreSpy.Message.insert)
      throw new Error()
    })
  }
}

export namespace CacheStoreSpy {
  export enum Message {
    delete,
    insert
  }
}
