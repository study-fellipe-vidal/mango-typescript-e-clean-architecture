import { CacheStore } from '@/data/protocols/cache'
import { SavePurchases } from '@/domain/usecases'

class LocalSavePurchases implements SavePurchases {
  constructor(
    private readonly cacheStore: CacheStore,
    private readonly timestamp: Date
  ) {}

  async save(purchases: Array<SavePurchases.Input>): Promise<void> {
    await this.cacheStore.replace('purchases', {
      timestamp: this.timestamp,
      value: purchases
    })
  }
}

export { LocalSavePurchases }
