import { CacheStoreSpy, mockPurchases } from '@/data/tests'
import { LocalSavePurchases } from '@/data/usecases'

type SutTypes = {
  sut: LocalSavePurchases
  cacheStore: CacheStoreSpy
}

const makeSut = (timestamp = new Date()): SutTypes => {
  const cacheStore = new CacheStoreSpy()
  const sut = new LocalSavePurchases(cacheStore, timestamp)
  return { sut, cacheStore }
}

describe('LocalSavePurchases', () => {
  it('Should not delete nor insert Cache on init', () => {
    const { cacheStore } = makeSut()

    expect(cacheStore.messages).toEqual([])
  })

  it('Should not insert new cache if delete fails', async () => {
    expect.assertions(2)
    const { sut, cacheStore } = makeSut()
    cacheStore.throwDeleteError()

    const promise = sut.save(mockPurchases())

    await expect(promise).rejects.toThrow()
    expect(cacheStore.messages).toEqual([CacheStoreSpy.Message.delete])
  })

  it('Should should delete old data from Cache and insert new data to Cache', async () => {
    expect.assertions(5)
    const timestamp = new Date()
    const { sut, cacheStore } = makeSut()
    const purchases = mockPurchases()

    const promise = sut.save(purchases)

    await expect(promise).resolves.toBeFalsy()
    expect(cacheStore.messages).toEqual([
      CacheStoreSpy.Message.delete,
      CacheStoreSpy.Message.insert
    ])
    expect(cacheStore.deleteKey).toBe('purchases')
    expect(cacheStore.insertKey).toBe('purchases')
    expect(cacheStore.insertValues).toEqual({ timestamp, value: purchases })
  })

  it('Should throw if insert throws', async () => {
    expect.assertions(2)
    const { sut, cacheStore } = makeSut()
    cacheStore.throwInsertError()

    const promise = sut.save(mockPurchases())

    await expect(promise).rejects.toThrow()
    expect(cacheStore.messages).toEqual([
      CacheStoreSpy.Message.delete,
      CacheStoreSpy.Message.insert
    ])
  })
})
