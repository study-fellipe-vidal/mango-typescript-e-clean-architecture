export namespace SavePurchases {
  export type Input = {
    id: string
    data: Date
    value: number
  }
}

export interface SavePurchases {
  save: (purchases: SavePurchases.Input[]) => Promise<void>
}
