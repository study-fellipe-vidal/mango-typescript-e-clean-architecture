# Gravar Compras no Cache

> ## Caso de sucesso

1. ✔ Sistema executa o comando "Salvar Compras"
2. Sistema cria uma data para ser armazenada no Cache
3. ✔ Sistema apaga os dados do Cache atual
4. ✔ Sistema grava on novos dados no Cache
5. Sistema não retorna nenhum erro

> ## Exceção - Erro ao apagar dados do Cache

3. ✔ Sistema retorna error
4. ✔ Sistema não grava os novos dados no Cache

> ## Exceção - Erro ao salvar dados no Cache

4. ✔ Sistema retorna error
